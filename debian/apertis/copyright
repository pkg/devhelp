Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: .editorconfig
Copyright: 2021, Emmanuele Bassi
License: CC0-1.0

Files: contrib/*
Copyright: 2004, Marc Britten <mbritten@monochromatic.net>
License: GPL-2+

Files: data/*
Copyright: 2001, Johan Dahlin <zilch.am@home.se> and Copyright2015 Frederic Peters <fpeters@0d.be>.
License: GPL

Files: debian/*
Copyright: 2001 - 2003 Mikael Hallendal <micke@imendio.com>,
            2001 - 2003 CodeFactory AB,
            2004 - 2008 Imendio AB,
            2008 Sven Herzberg
            2010 Lanedo GmbH
License: GPL-2+

Files: debian/devhelp.1
Copyright: 2001, Johan Dahlin <zilch.am@home.se>.
 2000, 2001, The Free Software Foundation
License: GPL

Files: debian/tests/libdevhelp-dev
Copyright: 2020, Simon McVittie
 2020, Collabora Ltd.
License: GPL-2

Files: devhelp/*
Copyright: 2017-2020, Sébastien Wilmet <swilmet@gnome.org>
License: GPL-3

Files: devhelp/devhelp.h
Copyright: 2012, Aleksander Morgado <aleksander@gnu.org>
License: GPL-3

Files: devhelp/dh-assistant-view.c
Copyright: 2008, Sven Herzberg
 2008, Imendio AB
License: GPL-3

Files: devhelp/dh-assistant-view.h
Copyright: 2008, Sven Herzberg
License: GPL-3

Files: devhelp/dh-book-manager.c
Copyright: 2017, 2018, Sébastien Wilmet <swilmet@gnome.org>
 2012, Thomas Bechtold <toabctl@gnome.org>
 2010, Lanedo GmbH
 2004-2008, Imendio AB
 2002, Mikael Hallendal <micke@imendio.com>
 2002, CodeFactory AB
License: GPL-3

Files: devhelp/dh-book-manager.h
Copyright: 2017, 2018, Sébastien Wilmet <swilmet@gnome.org>
 2010, Lanedo GmbH
License: GPL-3

Files: devhelp/dh-book-tree.c
Copyright: 2015, 2017, 2018, Sébastien Wilmet <swilmet@gnome.org>
 2010, Lanedo GmbH
 2008, Imendio AB
 2003, CodeFactory AB
 2001-2003, Mikael Hallendal <micke@imendio.com>
License: GPL-3

Files: devhelp/dh-book-tree.h
Copyright: 2018, Sébastien Wilmet <swilmet@gnome.org>
 2001, Mikael Hallendal <micke@imendio.com>
License: GPL-3

Files: devhelp/dh-book.c
 devhelp/dh-book.h
 devhelp/dh-keyword-model.c
Copyright: 2015-2018, Sébastien Wilmet <swilmet@gnome.org>
 2010, Lanedo GmbH
 2004-2008, Imendio AB
 2002, Mikael Hallendal <micke@imendio.com>
 2002, CodeFactory AB
License: GPL-3

Files: devhelp/dh-error.c
 devhelp/dh-error.h
 devhelp/dh-keyword-model.h
 devhelp/dh-parser.h
Copyright: 2017, 2018, Sébastien Wilmet <swilmet@gnome.org>
 2002, 2003, Mikael Hallendal <micke@imendio.com>
 2002, 2003, CodeFactory AB
License: GPL-3

Files: devhelp/dh-init.c
Copyright: 2017-2020, Sébastien Wilmet <swilmet@gnome.org>
 2012, Aleksander Morgado <aleksander@gnu.org>
License: GPL-3

Files: devhelp/dh-link.c
 devhelp/dh-link.h
 devhelp/dh-util-lib.c
 devhelp/dh-util-lib.h
Copyright: 2015, 2017, 2018, Sébastien Wilmet <swilmet@gnome.org>
 2004, 2008, Imendio AB
 2001, 2002, Mikael Hallendal <micke@imendio.com>
License: GPL-3

Files: devhelp/dh-parser.c
Copyright: 2017, 2018, Sébastien Wilmet <swilmet@gnome.org>
 2005, 2008, Imendio AB
 2002, 2003, Mikael Hallendal <micke@imendio.com>
 2002, 2003, CodeFactory AB
License: GPL-3

Files: devhelp/dh-settings.c
 devhelp/dh-settings.h
Copyright: 2017, 2018, Sébastien Wilmet <swilmet@gnome.org>
 2012, Thomas Bechtold <toabctl@gnome.org>
License: GPL-3

Files: devhelp/dh-sidebar.c
Copyright: 2015, 2017, 2018, Sébastien Wilmet <swilmet@gnome.org>
 2013, Aleksander Morgado <aleksander@gnu.org>
 2010, Lanedo GmbH
 2005-2008, Imendio AB
 2001-2003, Mikael Hallendal <micke@imendio.com>
 2001-2003, CodeFactory AB
License: GPL-3

Files: devhelp/dh-sidebar.h
Copyright: 2017, 2018, Sébastien Wilmet <swilmet@gnome.org>
 2013, Aleksander Morgado <aleksander@gnu.org>
 2001, 2002, Mikael Hallendal <micke@imendio.com>
 2001, 2002, CodeFactory AB
License: GPL-3

Files: help/*
Copyright: 2018-2022, devhelps COPYRIGHT HOLDER
License: UNKNOWN

Files: help/C/*
Copyright: 2018, 2019, Sébastien Wilmet <swilmet@gnome.org>
License: CC-BY-SA-4.0

Files: help/hu/*
Copyright: 2018-2021, Free Software Foundation, Inc.
License: UNKNOWN

Files: help/it/*
Copyright: 2018-2021, Free Software Foundation, Inc.
License: UNKNOWN

Files: help/pl/*
Copyright: 2018-2020, the devhelp authors.
License: UNKNOWN

Files: help/sv/*
Copyright: 2018-2021, Free Software Foundation, Inc.
License: UNKNOWN

Files: plugins/*
Copyright: 2008, Jannis Pohlmann <jannis@xfce.org>
License: GPL-3+

Files: plugins/gedit-plugin/*
Copyright: 2006, Richard Hult
License: UNKNOWN

Files: plugins/gedit-plugin/devhelp.py
Copyright: 2011, Red Hat, Inc.
 2006, Imendio AB
License: GPL-3+

Files: src/*
Copyright: 2017, 2018, Sébastien Wilmet <swilmet@gnome.org>
 2012, Thomas Bechtold <toabctl@gnome.org>
License: GPL-3

Files: src/dh-app.c
Copyright: 2017, 2018, Sébastien Wilmet <swilmet@gnome.org>
 2012, Aleksander Morgado <aleksander@gnu.org>
 2004-2008, Imendio AB
 2002, Mikael Hallendal <micke@imendio.com>
 2002, CodeFactory AB
License: GPL-3

Files: src/dh-app.h
Copyright: 2017-2020, Sébastien Wilmet <swilmet@gnome.org>
 2012, Aleksander Morgado <aleksander@gnu.org>
License: GPL-3

Files: src/dh-assistant.c
 src/dh-assistant.h
Copyright: 2008, Imendio AB
License: GPL-3

Files: src/dh-assistant.ui
Copyright: 2018, Sébastien Wilmet <swilmet@gnome.org>
 2013, Ignacio Casal Quinteiro <ignacio.casal@nice-software.com>
License: GPL-3

Files: src/dh-main.c
Copyright: 2001-2008, Imendio AB
 2001-2003, CodeFactory AB
License: GPL-3

Files: src/dh-preferences.c
Copyright: 2018, Sébastien Wilmet <swilmet@gnome.org>
 2012, Thomas Bechtold <toabctl@gnome.org>
 2010, Lanedo GmbH
 2004-2008, Imendio AB
License: GPL-3

Files: src/dh-preferences.h
Copyright: 2010, Lanedo GmbH
 2004-2008, Imendio AB
License: GPL-3

Files: src/dh-preferences.ui
Copyright: 2012, Aleksander Morgado <aleksander@gnu.org>
 2010, Imendio AB
License: GPL-3

Files: help/* help/hu/* help/it/* help/pl/* help/sv/* plugins/gedit-plugin/*
Copyright: 2001 - 2003 Mikael Hallendal <micke@imendio.com>,
 2001 - 2003 CodeFactory AB,
 2004 - 2008 Imendio AB,
 2008 Sven Herzberg
 2010 Lanedo GmbH
License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General Public
 License version 2 can be found in "/usr/share/common-licenses/GPL-2".

Files: src/dh-util-app.c
 src/dh-util-app.h
Copyright: 2015, 2017, 2018, Sébastien Wilmet <swilmet@gnome.org>
 2004, 2008, Imendio AB
 2001, 2002, Mikael Hallendal <micke@imendio.com>
License: GPL-3

Files: src/dh-window.c
Copyright: 2015-2020, Sébastien Wilmet <swilmet@gnome.org>
 2012, Thomas Bechtold <toabctl@gnome.org>
 2012, Aleksander Morgado <aleksander@gnu.org>
 2001-2008, Imendio AB
License: GPL-3

Files: src/dh-window.h
Copyright: 2017, 2018, Sébastien Wilmet <swilmet@gnome.org>
 2005, Imendio AB
 2002, CodeFactory AB
 2001, 2002, Mikael Hallendal <micke@imendio.com>
License: GPL-3

Files: src/dh.gresource.xml
Copyright: 2018, Sébastien Wilmet <swilmet@gnome.org>
 2018, Günther Wagner <info@gunibert.de>
 2016, Frédéric Péters <fpeters@0d.be>
 2013, Thomas Bechtold <thomasbechtold@jpberlin.de>
 2013, Ignacio Casal Quinteiro <ignacio.casal@nice-software.com>
License: GPL-3

Files: src/meson.build
Copyright: 2017-2020, Sébastien Wilmet <swilmet@gnome.org>
License: GPL-3

Files: unit-tests/*
Copyright: 2017-2020, Sébastien Wilmet <swilmet@gnome.org>
License: GPL-3

